import numpy as np

class Score:
    def __init__(self,index, matrice, mot, methode):
        #print("otniel ici")
        #tuple on veut pas que sa bouge c inchangeable
        self.answers = []
        self.index = index
        self.matrice = matrice
        self.mot = mot
        self.methode = methode
        '''
        tableaux de mot a eviter par mon analyseur de txt qui ne sont pas de synonyme
        
        '''
        l = ['ses','comme','était','votre','ah','est','même','avec','dit','?','mais','ai','m','nous','ou','cela','j','à','!','de','vous','moi','met','me','test','le','la','des','tu','qui','que','je','il','mon','ma','t','un','et','les','une','l', 'a', 'des','en','s','se','elle','on','avait','au','d','dans','ce','etait','qu','ne', 'du','pas','lui','son','sa','ils','pour','n','c','sur','plus','tout','autre','si','cette']
        self.stopList = {}
        for stopmot in l:
            self.stopList[stopmot] = 1
        

    #je dois calculer mon score    
    def scoreCalc(self):
        for mot, index in self.index.items():#prend utilise les mot comme cle du dictionnaire afin de parcourir ma matrice de coocurrence
            if mot != self.mot and mot not in self.stopList:
                score = self.prediction(self.matrice[index])
                self.answers.append( (mot, score) )
        self.descASC()
        
    def prediction(self,v):
        #m est mon meme vecteur je veux que v chance seulement
        if self.methode == "0":
            #print(self.index[i])
            #print(i.index)
            return self.produitScalaire(self.matrice[self.index[self.mot]],v)
            
        elif self.methode == "1":
            return self.leastSquare(self.matrice[self.index[self.mot]],v)
            
        elif self.methode == "2":
            return self.cityBlock(self.matrice[self.index[self.mot]],v)
        
    def produitScalaire(self, u, v):
        return np.dot(u, v) 
    def leastSquare(self, u, v):
        return np.sum(np.square(u-v))
    def cityBlock(self, u, v):
        return np.sum(np.absolute(u-v))#retour pas de negative
    
    '''
    def leasSquareM(m,v):
        return [np.sum(v) for v in np.square(m-v)]
    
    def cityBlockM(m,v):
        return [np.sum(v) for v in np.absolute(m-v)]#retourne une liste
    '''

    
    def descASC(self):
        inverser = False
        if self.methode == "0":
            inverser = True
        self.answers = sorted(self.answers, key=lambda t: t[1], reverse=inverser)


